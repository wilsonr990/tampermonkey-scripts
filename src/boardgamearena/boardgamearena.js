// ==UserScript==
// @name         boardgamearena games
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://boardgamearena.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=boardgamearena.com
// @grant        none
// @noframes
// ==/UserScript==

window.refreshTime = 10
window.refreshCount = 100

window.$x = function (xpathToExecute) {
    const result = []
    const nodesSnapshot = document.evaluate(xpathToExecute, document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null)
    for (let i = 0; i < nodesSnapshot.snapshotLength; i++) {
        result.push(nodesSnapshot.snapshotItem(i))
    }
    return result
}

window.elementIsVisibleByUser = (elem) => {
    if (!(elem instanceof Element)) throw Error('DomUtil: elem is not an element.')
    const style = getComputedStyle(elem)
    if (style.display === 'none') return false
    if (style.visibility !== 'visible') return false
    if (style.opacity < 0.1) return false
    if (elem.offsetWidth + elem.offsetHeight + elem.getBoundingClientRect().height + elem.getBoundingClientRect().width === 0) {
        return false
    }
    const elemCenter = {
        x: elem.getBoundingClientRect().left + elem.offsetWidth / 2,
        y: elem.getBoundingClientRect().top + elem.offsetHeight / 2
    }
    if (elemCenter.x < 0) return false
    if (elemCenter.x > (document.documentElement.clientWidth || window.innerWidth)) return false
    if (elemCenter.y < 0) return false
    if (elemCenter.y > (document.documentElement.clientHeight || window.innerHeight)) return false
    const pointContainer = document.elementFromPoint(elemCenter.x, elemCenter.y)
    do {
        if (pointContainer === elem) return true
    } while (pointContainer === pointContainer.parentNode)
    return false
}

window.insertElement = (parent, tag, id, style) => {
    const newE = document.createElement(tag)
    newE.id = id
    newE.style = style
    return parent.appendChild(newE)
}

window.removeElement = (id) => {
    document.body.removeChild(document.getElementById(id))
}

window.hasSingleElementWithText = (tag, text) => $x(`//${tag}[contains(text(), "${text}")]`).filter((el) => elementIsVisibleByUser(el)).length === 1

window.hasElementWithText = (tag, text) => $x(`//${tag}[contains(text(), "${text}")]`).filter((el) => elementIsVisibleByUser(el)).length > 0

window.getElementWithText = (tag, text) => $x(`//${tag}[contains(text(), "${text}")]`).filter((el) => elementIsVisibleByUser(el))[0]

window.getElementsWithText = (tag, text) => $x(`//${tag}[contains(text(), "${text}")]`).filter((el) => elementIsVisibleByUser(el))

window.hasSingleElementWithClass = (tag, classs) => $x(`//${tag}[contains(@class,"${classs}")]`).filter((el) => elementIsVisibleByUser(el)).length === 1

window.hasElementsWithClass = (tag, classs) => $x(`//${tag}[contains(@class,"${classs}")]`).length !== 0 // .filter(el => elementIsVisibleByUser(el))

window.hasVisibleElementsWithClass = (tag, classs) => $x(`//${tag}[contains(@class,"${classs}")]`).filter((el) => elementIsVisibleByUser(el)).length !== 0

window.getElementWithClass = (tag, classs) => $x(`//${tag}[contains(@class,"${classs}")]`).filter((el) => elementIsVisibleByUser(el))[0]

window.getElementsWithClass = (tag, classs) => $x(`//${tag}[contains(@class,"${classs}")]`).filter((el) => elementIsVisibleByUser(el))

window.getAllElementsWithClass = (tag, classs) => $x(`//${tag}[contains(@class,"${classs}")]`)

window.hasElementsWithClassAndStyle = (tag, classs, style) => {
    const spacedStyle = style.replace(':', ': ')
    return $x(`//${tag}[contains(@class,"${classs}") and (contains(@style,"${style}") or contains(@style,"${spacedStyle}"))]`).filter((el) => elementIsVisibleByUser(el)).length !== 0
}

window.hasAtLeastNElementsWithClassAndStyle = (n, tag, classs, style) => {
    const spacedStyle = style.replace(':', ': ')
    return $x(`//${tag}[contains(@class,"${classs}") and (contains(@style,"${style}") or contains(@style,"${spacedStyle}"))]`).filter((el) => elementIsVisibleByUser(el)).length >= n
}

window.getElementWithClassAndStyle = (tag, classs, style) => {
    const spacedStyle = style.replace(':', ': ')
    return $x(`//${tag}[contains(@class,"${classs}") and (contains(@style,"${style}") or contains(@style,"${spacedStyle}"))]`).filter((el) => elementIsVisibleByUser(el))[0]
}

window.getElementsWithClassAndStyle = (tag, classs, style) => {
    const spacedStyle = style.replace(':', ': ')
    return $x(`//${tag}[contains(@class,"${classs}") and (contains(@style,"${style}") or contains(@style,"${spacedStyle}"))]`).filter((el) => elementIsVisibleByUser(el))
}

window.hasElementsWithClassesAndStyle = (tag, classs1, classs2, style) => {
    const spacedStyle = style.replace(':', ': ')
    return $x(`//${tag}[contains(@class,"${classs1}") and contains(@class,"${classs2}") and (contains(@style,"${style}") or contains(@style,"${spacedStyle}"))]`).filter((el) => elementIsVisibleByUser(el)).length !== 0
}

window.getElementWithClassesAndStyle = (tag, classs1, classs2, style) => {
    const spacedStyle = style.replace(':', ': ')
    return $x(`//${tag}[contains(@class,"${classs1}") and contains(@class,"${classs2}") and (contains(@style,"${style}") or contains(@style,"${spacedStyle}"))]`).filter((el) => elementIsVisibleByUser(el))[0]
}

window.hasSingleElementWithId = (tag, id) => $x(`//${tag}[@id="${id}"]`).filter((el) => elementIsVisibleByUser(el)).length === 1

window.getElementWithId = (tag, id) => $x(`//${tag}[@id="${id}"]`).filter((el) => elementIsVisibleByUser(el))[0]

window.hasElementWithTextFromSibling = (tagp, textp, tagc, textc) => $x(`//*[${tagp}[contains(text(),"${textp}")]]//${tagc}[contains(text(), "${textc}")]`).filter((el) => elementIsVisibleByUser(el)).length === 1

window.getElementWithTextFromSibling = (tagp, textp, tagc, textc) => $x(`//*[${tagp}[contains(text(),"${textp}")]]//${tagc}[contains(text(), "${textc}")]`).filter((el) => elementIsVisibleByUser(el))[0];

(function () {
    // utils
    let current = 'started'
    let counter = 0

    window.game7wa = {
        cards: {
            MATH: '-300% -200%',
            GEAR: '-400% -200%',
            READ: '-500% -200%',
            GOLD: '-500% -100%',
            VIAL: '-400% -100%',
            PAPER: '-300% -100%',
            CLAY: '-200% -100%',
            STONE: '-100% -100%',
            WOOD: '-0% -100%',
            WOOD2: '0% -100%',
            POINTS3: '-700% -100%',
            CAT: '-600% -100%',
            ATCK: '-0% -200%',
            ATCK2: '0% -200%',
            SHIELD1: '-100% -200%',
            SHIELD2: '-200% -200%'
        },
        progressCards: {
            CARD_PER_GREEN: '-100% -0%',
            CARD_PER_GREEN2: '-100% 0%',
            CARD_PER_PYRAMID: '-300% -100%',
            DOUBLE_GOLD: '-100% -100%',
            CARD_PER_GOLD_AND_STONE: '-0% -100%',
            CARD_PER_GOLD_AND_STONE2: '0% -100%',
            CARD_PER_PAPER_AND_VIAL: '-300% -300%',
            CARD_PER_CLAY_AND_WOOD: '-300% -0%',
            CARD_PER_CLAY_AND_WOOD2: '-300% 0%',
            IGNORE_DIFF: '-200% -300%',
            POINTS_PER_PROGRESS: '-0% -300%',
            POINTS_PER_PYRAMID: '-200% -100%',
            POINTS_PER_THIS_CARD: '-100% -300%',
            POINTS_PER_CAT: '-200% -0%',
            POINTS_PER_CAT2: '-200% 0%',
            CARD_PER_DEFENSE: '-200% -200%',
            POINTS_PER_ATCK: '-0% -200%',
            POINTS_PER_ATCK2: '0% -200%',
            TWO_ATTACK: '-100% -200%'
        },
        icons: {
            GOLD: 'icon_gold',
            WOOD: 'icon_wood',
            STONE: 'icon_stone',
            CLAY: 'icon_clay',
            PAPER: 'icon_paper',
            VIAL: 'icon_vial',
            READ: 'icon_read',
            GEAR: 'icon_gear',
            MATH: 'icon_math',
            WAR: 'icon_war',
            SHIELD: 'icon_shield',
            VP: 'icon_VP'
        },
        gettingAdditionalCards: '',
        hasProgressCard: (card) => {
            const hasCard = hasElementsWithClassAndStyle("div[contains(@class, 'mainarea')]//*", 'progress', `background-position:${game7wa.progressCards[card]}`)
            const isEnabled = hasCard && !getElementWithClassAndStyle("div[contains(@class, 'mainarea')]//*", 'progress', `background-position:${game7wa.progressCards[card]}`).className.includes('disabled')
            return isEnabled
        },
        enemyHasProgressCard: (card) => hasElementsWithClassAndStyle("div[contains(@class, 'otherarea')]//*", 'progress', `background-position:${game7wa.progressCards[card]}`),
        hasIcon: (icon) => hasElementsWithClass("div[contains(@class, 'mainarea')]//*", `stw_area_icon ${game7wa.icons[icon]}`),
        enemyHasIcon: (icon) => hasElementsWithClass("div[contains(@class, 'otherarea')]//*", `stw_area_icon ${game7wa.icons[icon]}`),
        horns: () => {
            let horns = 0
            if (hasElementsWithClass('*', 'token horn')) {
                horns = getElementsWithClass('*', 'token horn flipped').length
            }
            return horns
        },
        totalAttack: () => {
            let attack = 0
            if (game7wa.hasIcon('SHIELD') && game7wa.hasIcon('WAR')) {
                const bothElements = getAllElementsWithClass("div[contains(@class, 'mainarea')]//*", `stw_area_icon ${game7wa.icons.SHIELD}`)[0].parentElement.children
                attack += Number.parseInt(`0${bothElements[0].parentElement.children[0].innerText}`) + Number.parseInt(`0${bothElements[1].parentElement.children[0].innerText}`)
            } else if (game7wa.hasIcon('SHIELD')) {
                const element = getAllElementsWithClass("div[contains(@class, 'mainarea')]//*", `stw_area_icon ${game7wa.icons.SHIELD}`)[0].parentElement.children
                attack += Number.parseInt(`0${element[0].innerText}`)
            } else if (game7wa.hasIcon('WAR')) {
                const element = getAllElementsWithClass("div[contains(@class, 'mainarea')]//*", `stw_area_icon ${game7wa.icons.WAR}`)[0].parentElement.children
                attack += Number.parseInt(`0${element[0].innerText}`)
            }
            return attack
        },
        enemyTotalAttack: () => {
            let attack = 0
            if (game7wa.hasIcon('SHIELD') && game7wa.hasIcon('WAR')) {
                const bothElements = getAllElementsWithClass("div[contains(@class, 'mainarea')]//*", `stw_area_icon ${game7wa.icons.SHIELD}`)[0].parentElement.children
                attack += Number.parseInt(`0${bothElements[0].parentElement.children[0].innerText}`) + Number.parseInt(`0${bothElements[1].parentElement.children[0].innerText}`)
            } else if (game7wa.enemyHasIcon('SHIELD')) {
                const element = getAllElementsWithClass("div[contains(@class, 'otherarea')]//*", `stw_area_icon ${game7wa.icons.SHIELD}`)[0].parentElement.children
                attack += Number.parseInt(`0${element[0].innerText}`)
            } else if (game7wa.enemyHasIcon('WAR')) {
                const element = getAllElementsWithClass("div[contains(@class, 'otherarea')]//*", `stw_area_icon ${game7wa.icons.WAR}`)[0].parentElement.children
                attack += Number.parseInt(`0${element[0].innerText}`)
            }
            return attack
        },
        myWonderType: () => {
            if (hasElementsWithClass("div[contains(@class, 'mainarea')]//*", 'wonderpos_1 wonder_')) {
                return "pyramid"
            }
            return "unknown"
        },
        enemyWonderType: () => {
            if (hasElementsWithClass("div[contains(@class, 'otherarea')]//*", 'wonderpos_1 wonder_')) {
                return "pyramid"
            }
            return "unknown"
        },
        currentWonder: () => 6 - getAllElementsWithClass("div[contains(@class, 'mainarea')]//*", 'arc_wonder turned').length,
        enemyCurrentWonder: () => 6 - getAllElementsWithClass("div[contains(@class, 'otherarea')]//*", 'arc_wonder turned').length,
        hasCat: () => {
            const hasCard = hasSingleElementWithClass('div[contains(@class, "mainarea")]//*', 'iscat')
            const isEnabled = hasCard && !getElementWithClass('div[contains(@class, "mainarea")]//*', 'iscat').className.includes('disabled')
            return isEnabled
        },
        selectCard: (card, n = 1) => {
            if (hasAtLeastNElementsWithClassAndStyle(n, '*', 'canselect', `background-position:${game7wa.cards[card]}`)) {
                console.log(`DEBUG >>>>>>>>> selecting ${n} of ${card}`)
                for (let i = 0; i < n; i++) {
                    getElementsWithClassAndStyle('*', 'canselect', `background-position:${game7wa.cards[card]}`)[i].click()
                }
                return true
            }
            return false
        },
        selectProgressCard: (card) => {
            if (hasElementsWithClassesAndStyle('*', 'progress', 'canselect', `background-position:${game7wa.progressCards[card]}`)) {
                getElementWithClassesAndStyle('*', 'progress', 'canselect', `background-position:${game7wa.progressCards[card]}`).click()
                return true
            }
            return false
        }
    }

    window.cantstop = {
        retry: 0,
        retryLimit: 3,
        max: {2: 3, 3: 5, 4: 7, 5: 9, 6: 11, 7: 13, 8: 11, 9: 9, 10: 7, 11: 5, 12: 3},
        selected: [],
        temp_progress: {2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0, 12: 0},
        total_progress: 0,
        progress: {2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0, 12: 0},
        addProgress: (string) => {
            getElementWithText('*', string).text.matchAll("[0-9]+")
                .toArray().forEach(n => {
                if (!cantstop.selected.includes(n[0])) {
                    cantstop.selected.push(n[0])
                }
                cantstop.temp_progress[n] = cantstop.temp_progress[n] + 1
                cantstop.total_progress += 1 / cantstop.max[n]
            })
        },
        finishedColumn: () => {
            let finished = 0
            Array(12).fill(0).map((_, index) => index)
                .forEach(n => {
                    if (cantstop.temp_progress[n] >= cantstop.max[n]) {
                        finished = finished + 1
                    }
                })
            if (finished > 0 && finished < 3) {
                return cantstop.selected.length === 3
            } else if (finished === 3) {
                return true
            }
            return false
        },
        bestElementString: () => {
            let bestScore = 0
            let bestText = ""
            getElementsWithText('*', 'Progresses on')
                .forEach(element => {
                    const text = element.text
                    const matches = text.matchAll("[0-9]+").toArray()
                    let score = matches
                        .map(n => {
                            if (cantstop.temp_progress[n[0]] + 1 >= cantstop.max[n[0]]) return 1
                            return 0.334 * (1 - Math.abs((7 - n[0])) / 5) + 0.666 * (cantstop.temp_progress[n[0]] + 1) / cantstop.max[n[0]]
                        })
                        .reduce((partialSum, a) => partialSum + a, 0) / matches.length

                    const progress = matches
                        .map(n => 1 / cantstop.max[n[0]]).reduce((sum, a) => sum + a, 0)
                    const min_expected_progress = 1.2 * cantstop.selected.map(n => cantstop.max[n] / 13).reduce((sum, a) => sum + a, 0) / cantstop.selected.length
                    const aboutToFinishColumn = matches
                        .map(n => (cantstop.temp_progress[n[0]] + 2) >= cantstop.max[n[0]]).reduce((res, a) => a ? a : res, false);
                    let chill = ""
                    if (cantstop.total_progress + progress < min_expected_progress && aboutToFinishColumn) {
                        score = score * 0.9
                        chill = "chill"
                    }

                    if (score > bestScore) {
                        bestScore = score
                        bestText = text
                    }
                    window.actions = [text.replace("Progresses on", "") + " - " + Math.round(score * 100) + "% " + chill, ...window.actions]
                })
            window.actions = [" > " + bestText.replace("Progresses on", ""), ...window.actions]
            return bestText
        }
    }

    botbox = window.insertElement(document.body, 'div', 'botdata', `
        background-color: red;
        position: fixed;
        top: 0px;
        right: 0px;
        z-index: 9999;
        margin: 10px;
        padding: 20px;
        min-width: 100px;`)
    playButton = window.insertElement(botbox, 'button', 'play', {})
    playButton.innerText = "play"
    playButton.onclick = () => {
        window.start_game = true
        current = 'choosingGame'
    }
    botlabel = window.insertElement(botbox, 'label', 'label', {})

    window.actions = []

    window.states = {
        started: {
            trigger: () => {
                if (document.title.includes('Searching for ') && document.title.includes('opponent...')) {
                    current = 'waitingGameStart'
                } else if (document.location.toString().includes('/lobby')) {
                    current = 'choosingGame'
                } else if (document.location.toString().includes('/welcome') || document.location.pathname === '/' || document.location.toString().includes('/table?table=')) {
                    current = 'goPlayNow'
                } else if (document.location.toString().includes('?table=')) {
                    current = 'playing'
                }
            }
        },
        goPlayNow: {
            trigger: () => {
                if (hasSingleElementWithText('a', 'Play now')) {
                    getElementWithText('a', 'Play now').click()
                    current = 'choosingGame'
                }
            }
        },
        choosingGame: {
            trigger: () => {
                window.actions = []
                if (window.start_game) {
                    // Add automatic and real time game type selection
                    if (hasElementWithTextFromSibling('*/*/a', "Can't Stop", '*', 'Play now!')) {
                        getElementWithTextFromSibling('*/*/a', "Can't Stop", '*', 'Play now!').click()
                        current = 'waitingGameStart'
                    }
                    if (hasElementWithTextFromSibling("*/*/a", "7 Wonders Architects", "*", "Play now!")) {
                        getElementWithTextFromSibling("*/*/a", "7 Wonders Architects", "*", "Play now!").click()
                        current = 'waitingGameStart'
                    }
                    if (hasElementWithTextFromSibling("*/*/a", "Martian Dice", "*", "Play now!")) {
                        getElementWithTextFromSibling("*/*/a", "Martian Dice", "*", "Play now!").click()
                        current = 'waitingGameStart'
                    }
                    if (hasElementWithTextFromSibling("*/*/a", "6 nimmt!", "*", "Play now!")) {
                        getElementWithTextFromSibling("*/*/a", "6 nimmt!", "*", "Play now!").click()
                        current = 'waitingGameStart'
                    }
                    window.start_game = false
                } else {
                    current = 'waitingGameStart'
                }
            }
        },
        waitingGameStart: {
            trigger: () => {
                window.actions = []
                if (hasSingleElementWithText('*', 'Accept')) {
                    getElementWithText('*', 'Accept').click()
                    current = 'playing'
                }
            }
        },
        waitingAction: {
            trigger: () => {
                botlabel.innerText = window.actions.join('\n')
                if ((document.title.includes('End of game') || document.title.includes('End of the game')) && hasSingleElementWithText('*', 'Return to main site')) {
                    getElementWithText('*', 'Return to main site').click()
                    current = 'started'
                } else {
                    if (hasSingleElementWithText('*', 'Expel')) {
                        getElementWithText('*', 'Expel').click()
                        current = 'started'
                    } else if (hasSingleElementWithId('*', 'neutralized_quit')) {
                        getElementWithId('*', 'neutralized_quit').click()
                        current = 'started'
                    } else if (hasSingleElementWithId('*', 'fireplayer_confirm')) {
                        getElementWithId('*', 'fireplayer_confirm').click()
                        current = 'started'
                    } else if (hasSingleElementWithText('*', 'Skip turn of players out of time')) {
                        getElementWithText('*', 'Skip turn of players out of time').click()
                        current = 'started'
                    } else if (hasSingleElementWithText('*', 'quit')) {
                        getElementWithText('*', 'quit').click()
                        current = 'started'
                    }
                }
            }
        },
        playing: {
            trigger: () => {
                if (hasSingleElementWithText('*', 'Cancel game start')) {
                    counter += 1
                    if (counter === 20000 / window.refreshTime) {
                        getElementWithText('*', 'Cancel game start').click()
                        current = 'started'
                        counter = 0
                    }
                } else {
                    let gameName = document.location.toString().split("?")[0].split("/").pop()
                    if (gameName && gameName !== "blank") {
                        current = gameName + '.waitingAction'
                        counter = 0
                    }
                    playButton.style.display = 'none'
                }
            }
        },
        sevenwondersarchitects: {
            waitingAction: {
                trigger: () => {
                    if ((document.title.includes('You must play') || !document.title.includes(' must play')) && (document.title.includes('You must select a card') || !document.title.includes(' must select a card')) && !hasSingleElementWithText('*', 'Updating game situation...') && hasSingleElementWithId('*', 'pagemaintitletext') && getElementWithId('*', 'pagemaintitletext').innerText.trim() !== '') {
                        console.log(`MESSAGE:${getElementWithId('*', 'pagemaintitletext').innerText}`)
                        current = 'sevenwondersarchitects.playAction'
                    }
                }
            },
            playAction: {
                trigger: () => {
                    if (document.title.includes('Please select 2 similar cards to spend.')) {
                        current = 'sevenwondersarchitects.select2SimilarResources'
                    } else if (document.title.includes('Please select 3 similar cards to spend.')) {
                        current = 'sevenwondersarchitects.select3SimilarResources'
                    } else if (document.title.includes('Please select 2 different cards to spend.')) {
                        current = 'sevenwondersarchitects.select2DifferentResources'
                    } else if (document.title.includes('Please select 3 different cards to spend.')) {
                        current = 'sevenwondersarchitects.select3DifferentResources'
                    } else if (document.title.includes('Please select 4 different cards to spend.')) {
                        current = 'sevenwondersarchitects.select4DifferentResources'
                    } else if (document.title.includes('Please select 2 similar cards')) {
                        current = 'sevenwondersarchitects.select2DifferentGreen'
                    } else if (hasElementsWithClassesAndStyle('*', 'progress', 'canselect', '')) {
                        current = 'sevenwondersarchitects.selectProgress'
                    } else if (hasElementsWithClass('*', 'canselect')) {
                        current = 'sevenwondersarchitects.selectCard'
                    } else {
                        current = 'sevenwondersarchitects.waitingAction'
                    }
                }
            },
            selectProgress: {
                trigger: () => {
                    if (game7wa.currentWonder() <= 3 && game7wa.selectProgressCard('CARD_PER_GREEN')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.currentWonder() <= 3 && game7wa.selectProgressCard('CARD_PER_GREEN2')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.currentWonder() <= 3 && game7wa.selectProgressCard('DOUBLE_GOLD')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.selectProgressCard('IGNORE_DIFF')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.currentWonder() <= 3 && game7wa.selectProgressCard('CARD_PER_GOLD_AND_STONE')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.currentWonder() <= 3 && game7wa.selectProgressCard('CARD_PER_GOLD_AND_STONE2')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.currentWonder() <= 3 && game7wa.selectProgressCard('CARD_PER_PAPER_AND_VIAL')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.currentWonder() <= 3 && game7wa.selectProgressCard('CARD_PER_CLAY_AND_WOOD')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.currentWonder() <= 3 && game7wa.selectProgressCard('CARD_PER_CLAY_AND_WOOD2')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.currentWonder() <= 2 && game7wa.selectProgressCard('CARD_PER_PYRAMID')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.selectProgressCard('POINTS_PER_PROGRESS')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.selectProgressCard('POINTS_PER_PYRAMID')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.selectProgressCard('POINTS_PER_THIS_CARD')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.selectProgressCard('POINTS_PER_CAT')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.selectProgressCard('POINTS_PER_CAT2')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.selectProgressCard('DOUBLE_GOLD')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.currentWonder() < 5 && game7wa.selectProgressCard('CARD_PER_GOLD_AND_STONE')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.currentWonder() < 5 && game7wa.selectProgressCard('CARD_PER_GOLD_AND_STONE2')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.currentWonder() < 5 && game7wa.selectProgressCard('CARD_PER_PAPER_AND_VIAL')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.currentWonder() < 5 && game7wa.selectProgressCard('CARD_PER_CLAY_AND_WOOD')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.currentWonder() < 5 && game7wa.selectProgressCard('CARD_PER_CLAY_AND_WOOD2')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (hasElementsWithClassesAndStyle('*', 'progress', 'canselect', '')) {
                        getElementWithClassesAndStyle('*', 'progress', 'canselect', '').click()
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.currentWonder() <= 2 && game7wa.selectProgressCard('CARD_PER_DEFENSE')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.selectProgressCard('POINTS_PER_ATCK') || game7wa.selectProgressCard('POINTS_PER_ATCK2')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    }
                }
            },
            selectCard: {
                trigger: () => {
                    let options = getElementsWithClass('*', 'canselect');
                    window.actions = ["options: " + options.map(it => {
                        let k = Object.keys(game7wa.cards).find(key => game7wa.cards[key] === it.style["background-position"])
                        if (k) {
                            return k
                        } else {
                            return it.style["background-position"]
                        }
                    }).join(', '), ...window.actions]

                    // play action that allow taking additional cards
                    if (game7wa.gettingAdditionalCards !== 'CARD_PER_GREEN' && (game7wa.hasProgressCard('CARD_PER_GREEN') || game7wa.hasProgressCard('CARD_PER_GREEN2')) && (game7wa.selectCard('MATH') || game7wa.selectCard('GEAR') || game7wa.selectCard('READ'))) {
                        current = 'sevenwondersarchitects.waitingAction'
                        game7wa.gettingAdditionalCards = 'CARD_PER_GREEN'
                    } else if (game7wa.gettingAdditionalCards !== 'CARD_PER_CLAY_AND_WOOD' && (game7wa.hasProgressCard('CARD_PER_CLAY_AND_WOOD') || game7wa.hasProgressCard('CARD_PER_CLAY_AND_WOOD2')) && (game7wa.selectCard('CLAY') || game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2'))) {
                        current = 'sevenwondersarchitects.waitingAction'
                        game7wa.gettingAdditionalCards = 'CARD_PER_CLAY_AND_WOOD'
                    } else if (game7wa.gettingAdditionalCards !== 'CARD_PER_PAPER_AND_VIAL' && game7wa.hasProgressCard('CARD_PER_PAPER_AND_VIAL') && (game7wa.selectCard('PAPER') || game7wa.selectCard('VIAL'))) {
                        current = 'sevenwondersarchitects.waitingAction'
                        game7wa.gettingAdditionalCards = 'CARD_PER_PAPER_AND_VIAL'
                    } else if (game7wa.gettingAdditionalCards !== 'CARD_PER_GOLD_AND_STONE' && (game7wa.hasProgressCard('CARD_PER_GOLD_AND_STONE') || game7wa.hasProgressCard('CARD_PER_GOLD_AND_STONE2')) && (game7wa.selectCard('GOLD') || game7wa.selectCard('STONE'))) {
                        current = 'sevenwondersarchitects.waitingAction'
                        game7wa.gettingAdditionalCards = 'CARD_PER_GOLD_AND_STONE'
                    } else if (game7wa.gettingAdditionalCards !== 'CARD_PER_DEFENSE' && game7wa.totalAttack() - game7wa.enemyTotalAttack() >= -1 && game7wa.hasProgressCard('CARD_PER_DEFENSE') && (game7wa.selectCard('SHIELD1') || game7wa.selectCard('SHIELD2'))) {
                        current = 'sevenwondersarchitects.waitingAction'
                        game7wa.gettingAdditionalCards = 'CARD_PER_DEFENSE'
                    }
                    if (current !== 'sevenwondersarchitects.selectCard') {
                        window.actions = ['Ensure additional cards', ...window.actions]
                        console.log(' >>>>>>>>> Ensure additional cards')
                        return
                    }
                    game7wa.gettingAdditionalCards = ''
                    // completes a progress pair using enemies requirement to complete a pair
                    if (game7wa.enemyHasIcon('MATH') && game7wa.hasIcon('MATH') && game7wa.selectCard('MATH')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.enemyHasIcon('GEAR') && game7wa.hasIcon('GEAR') && game7wa.selectCard('GEAR')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.enemyHasIcon('READ') && game7wa.hasIcon('READ') && game7wa.selectCard('READ')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    }

                    if (current !== 'sevenwondersarchitects.selectCard') {
                        window.actions = ['Completes a progress pair using enemies requirement', ...window.actions]
                        console.log(' >>>>>>>>> Completes a progress pair using enemies requirement to complete a pair')
                        return
                    }
                    // completes a progress pair
                    if (game7wa.hasIcon('MATH') && game7wa.selectCard('MATH')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.hasIcon('GEAR') && game7wa.selectCard('GEAR')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.hasIcon('READ') && game7wa.selectCard('READ')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    }
                    if (current !== 'sevenwondersarchitects.selectCard') {
                        window.actions = ['Completes a progress pair', ...window.actions]
                        console.log(' >>>>>>>>> Completes a progress pair')
                        return
                    }
                    // make sure opponent cannot complete a progress pair!
                    if (game7wa.currentWonder() !== 5 && game7wa.enemyCurrentWonder() !== 5) {
                        if (game7wa.enemyHasIcon('MATH') && game7wa.selectCard('MATH')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.enemyHasIcon('GEAR') && game7wa.selectCard('GEAR')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.enemyHasIcon('READ') && game7wa.selectCard('READ')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        }
                    }
                    if (current !== 'sevenwondersarchitects.selectCard') {
                        window.actions = ['Steals opponents progress card', ...window.actions]
                        console.log(' >>>>>>>>> Steals opponents needed progress card')
                        return
                    }
                    // make sure opponent cannot take additional cards if possible
                    if (game7wa.currentWonder() !== 5) {
                        if (game7wa.enemyCurrentWonder() === 5 && ((!game7wa.enemyHasIcon('VIAL') && game7wa.selectCard('VIAL')) || (!game7wa.enemyHasIcon('PAPER') && game7wa.selectCard('PAPER')) || (!game7wa.enemyHasIcon('CLAY') && game7wa.selectCard('CLAY')) || (!game7wa.enemyHasIcon('STONE') && game7wa.selectCard('STONE')) || (!game7wa.enemyHasIcon('WOOD') && game7wa.selectCard('WOOD')) || (!game7wa.enemyHasIcon('WOOD2') && game7wa.selectCard('WOOD2')))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if ((game7wa.enemyHasProgressCard('CARD_PER_GREEN') || game7wa.enemyHasProgressCard('CARD_PER_GREEN2')) && (game7wa.selectCard('MATH') || game7wa.selectCard('GEAR') || game7wa.selectCard('READ'))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if ((game7wa.enemyHasProgressCard('CARD_PER_CLAY_AND_WOOD') || game7wa.enemyHasProgressCard('CARD_PER_CLAY_AND_WOOD2')) && (game7wa.selectCard('CLAY') || game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2'))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.enemyHasProgressCard('CARD_PER_PAPER_AND_VIAL') && (game7wa.selectCard('PAPER') || game7wa.selectCard('VIAL'))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if ((game7wa.enemyHasProgressCard('CARD_PER_GOLD_AND_STONE') || game7wa.enemyHasProgressCard('CARD_PER_GOLD_AND_STONE2')) && (game7wa.selectCard('GOLD') || game7wa.selectCard('STONE'))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.totalAttack() - game7wa.enemyTotalAttack() >= -1 && game7wa.enemyHasProgressCard('CARD_PER_DEFENSE') && (game7wa.selectCard('SHIELD1') || game7wa.selectCard('SHIELD2'))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        }
                    }
                    if (current !== 'sevenwondersarchitects.selectCard') {
                        window.actions = ['Steals opponents extra card', ...window.actions]
                        console.log(' >>>>>>>>> Steals opponents needed extra card')
                        return
                    }
                    // make sure opponent cannot get needed Gold
                    if (game7wa.enemyHasProgressCard('DOUBLE_GOLD') && game7wa.selectCard('GOLD')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    }
                    if (current !== 'sevenwondersarchitects.selectCard') {
                        window.actions = ['Steals opponents gold', ...window.actions]
                        console.log(' >>>>>>>>> Steals opponents needed gold')
                        return
                    }
                    // Attack if can win!!!
                    if (game7wa.horns() === 2 && (game7wa.totalAttack() + 1 === game7wa.enemyTotalAttack() + 1 || game7wa.enemyTotalAttack() + 1 === 2 * game7wa.totalAttack()) && (game7wa.selectCard('SHIELD1') || game7wa.selectCard('SHIELD2'))) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.horns() === 1 && (game7wa.totalAttack() + 1 === game7wa.enemyTotalAttack() + 1 || game7wa.enemyTotalAttack() + 1 === 2 * game7wa.totalAttack()) && game7wa.selectCard('SHIELD1')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    }
                    if (current !== 'sevenwondersarchitects.selectCard') {
                        window.actions = ['Attack cause I can win', ...window.actions]
                        console.log(' >>>>>>>>> Attack if opponent is attacking. Or if can win attack')
                        return
                    }
                    // Flow in start of game
                    if (game7wa.currentWonder() <= 2 && game7wa.enemyCurrentWonder() <= 2 && game7wa.myWonderType() !== 'pyramid') {
                        if (((!game7wa.hasIcon('GEAR') || !game7wa.hasIcon('READ')) && game7wa.selectCard('MATH')) || ((!game7wa.hasIcon('MATH') || !game7wa.hasIcon('READ')) && game7wa.selectCard('GEAR')) || ((!game7wa.hasIcon('MATH') || !game7wa.hasIcon('GEAR')) && game7wa.selectCard('READ'))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.enemyTotalAttack() > 1 && game7wa.totalAttack() - game7wa.enemyTotalAttack() <= 0 && game7wa.totalAttack() - game7wa.enemyTotalAttack() > -3 && (game7wa.selectCard('ATCK') || game7wa.selectCard('ATCK2') /* || GAME_7wa.selectCard("SHIELD1") || GAME_7wa.selectCard("SHIELD2") */)) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('POINTS3')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('CAT')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (!game7wa.hasCat()) {
                            getElementWithClass('*', 'canselect').click()
                            current = 'sevenwondersarchitects.waitingAction'
                            console.log(' >>>>>>>>> Just take random')
                        } else if (game7wa.selectCard('GOLD')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('VIAL') || game7wa.selectCard('PAPER') || game7wa.selectCard('CLAY') || game7wa.selectCard('STONE') || game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('MATH') || game7wa.selectCard('GEAR') || game7wa.selectCard('READ')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        }
                    }
                    if (current !== 'sevenwondersarchitects.selectCard') {
                        if (!window.actions.includes('Start Of Game')) {
                            window.actions = ['Start Of Game', ...window.actions]
                        }
                        console.log(' >>>>>>>>> Start of Game')
                        return
                    }
                    // Flow in middle game. Enemy ahead
                    if (game7wa.currentWonder() < 4 && game7wa.enemyCurrentWonder() > game7wa.currentWonder() && game7wa.myWonderType() !== 'pyramid') {
                        if ((game7wa.hasIcon('GEAR') && game7wa.hasIcon('READ') && game7wa.selectCard('MATH')) || (game7wa.hasIcon('MATH') && game7wa.hasIcon('READ') && game7wa.selectCard('GEAR')) || (game7wa.hasIcon('MATH') && game7wa.hasIcon('GEAR') && game7wa.selectCard('READ'))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('GOLD')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.hasProgressCard('IGNORE_DIFF') && (game7wa.selectCard('VIAL') || game7wa.selectCard('PAPER') || game7wa.selectCard('CLAY') || game7wa.selectCard('STONE') || game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2'))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (((!game7wa.hasIcon('VIAL') && !game7wa.hasIcon('PAPER') && !game7wa.hasIcon('CLAY') && !game7wa.hasIcon('STONE') && !game7wa.hasIcon('WOOD'))) && ((game7wa.selectCard('VIAL') || game7wa.selectCard('PAPER') || game7wa.selectCard('CLAY') || game7wa.selectCard('STONE') || game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2')))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.currentWonder() === 1 && (game7wa.selectCard('VIAL') || game7wa.selectCard('PAPER') || game7wa.selectCard('CLAY') || game7wa.selectCard('STONE') || game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2'))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.currentWonder() === 2 && ((game7wa.hasIcon('VIAL') && game7wa.selectCard('VIAL')) || (game7wa.hasIcon('PAPER') && game7wa.selectCard('PAPER')) || (game7wa.hasIcon('CLAY') && game7wa.selectCard('CLAY')) || (game7wa.hasIcon('STONE') && game7wa.selectCard('STONE')) || (game7wa.hasIcon('WOOD') && (game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2'))))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.currentWonder() === 3 && ((!game7wa.hasIcon('VIAL') && game7wa.selectCard('VIAL')) || (!game7wa.hasIcon('PAPER') && game7wa.selectCard('PAPER')) || (!game7wa.hasIcon('CLAY') && game7wa.selectCard('CLAY')) || (!game7wa.hasIcon('STONE') && game7wa.selectCard('STONE')) || (!game7wa.hasIcon('WOOD') && (game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2'))))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (!game7wa.hasCat()) {
                            getElementWithClass('*', 'canselect').click()
                            current = 'sevenwondersarchitects.waitingAction'
                            console.log(' >>>>>>>>> Just take random')
                        } else if (!game7wa.hasCat() && game7wa.selectCard('CAT')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('POINTS3')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('CAT')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.enemyTotalAttack() !== 0 && game7wa.totalAttack() - game7wa.enemyTotalAttack() <= 0 && game7wa.totalAttack() - game7wa.enemyTotalAttack() > -3 && (game7wa.selectCard('SHIELD1') || game7wa.selectCard('SHIELD2'))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('MATH') || game7wa.selectCard('GEAR') || game7wa.selectCard('READ')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('VIAL') || game7wa.selectCard('PAPER') || game7wa.selectCard('CLAY') || game7wa.selectCard('STONE') || game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        }
                    }
                    if (current !== 'sevenwondersarchitects.selectCard') {
                        if (!window.actions.includes('Middle Game - EA')) window.actions = ['Middle Game - EA', ...window.actions]
                        console.log(' >>>>>>>>> Middle game - Enemy ahead!')
                        return
                    }
                    // Flow in middle game
                    if ((game7wa.currentWonder() === 3 || (game7wa.currentWonder() - game7wa.enemyCurrentWonder() >= 2)) && game7wa.myWonderType() !== 'pyramid') {
                        if (((!game7wa.hasIcon('GEAR') || !game7wa.hasIcon('READ')) && game7wa.selectCard('MATH')) || ((!game7wa.hasIcon('MATH') || !game7wa.hasIcon('READ')) && game7wa.selectCard('GEAR')) || ((!game7wa.hasIcon('MATH') || !game7wa.hasIcon('GEAR')) && game7wa.selectCard('READ'))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('GOLD')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (!game7wa.hasProgressCard('IGNORE_DIFF') && ((!game7wa.hasIcon('VIAL') && game7wa.selectCard('VIAL')) || (!game7wa.hasIcon('PAPER') && game7wa.selectCard('PAPER')) || (!game7wa.hasIcon('CLAY') && game7wa.selectCard('CLAY')) || (!game7wa.hasIcon('STONE') && game7wa.selectCard('STONE')) || (!game7wa.hasIcon('WOOD') && (game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2'))))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (!game7wa.hasCat() && game7wa.selectCard('CAT')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('POINTS3')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('CAT')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.enemyTotalAttack() !== 0 && game7wa.totalAttack() - game7wa.enemyTotalAttack() <= 0 && game7wa.totalAttack() - game7wa.enemyTotalAttack() > -2 && (game7wa.selectCard('SHIELD1') || game7wa.selectCard('SHIELD2'))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.hasProgressCard('IGNORE_DIFF') && (game7wa.selectCard('VIAL') || game7wa.selectCard('PAPER') || game7wa.selectCard('CLAY') || game7wa.selectCard('STONE') || game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2'))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (((!game7wa.hasIcon('VIAL') && game7wa.selectCard('VIAL')) || (!game7wa.hasIcon('PAPER') && game7wa.selectCard('PAPER')) || (!game7wa.hasIcon('CLAY') && game7wa.selectCard('CLAY')) || (!game7wa.hasIcon('STONE') && game7wa.selectCard('STONE')) || (!game7wa.hasIcon('WOOD') && (game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2'))))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (!game7wa.hasCat()) {
                            getElementWithClass('*', 'canselect').click()
                            current = 'sevenwondersarchitects.waitingAction'
                            console.log(' >>>>>>>>> Just take random')
                        } else if (game7wa.selectCard('MATH') || game7wa.selectCard('GEAR') || game7wa.selectCard('READ')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('VIAL') || game7wa.selectCard('PAPER') || game7wa.selectCard('CLAY') || game7wa.selectCard('STONE') || game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        }
                    }
                    if (current !== 'sevenwondersarchitects.selectCard') {
                        if (!window.actions.includes('Middle Game - EVEN')) window.actions = ['Middle Game - EVEN', ...window.actions]
                        console.log(' >>>>>>>>> Middle game even')
                        return
                    }
                    // Flow in the Advanced game
                    if (game7wa.currentWonder() === 4) {
                        if ((game7wa.hasIcon('GEAR') && game7wa.hasIcon('READ') && game7wa.selectCard('MATH')) || (game7wa.hasIcon('MATH') && game7wa.hasIcon('READ') && game7wa.selectCard('GEAR')) || (game7wa.hasIcon('MATH') && game7wa.hasIcon('GEAR') && game7wa.selectCard('READ'))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('GOLD')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.hasProgressCard('IGNORE_DIFF') && (game7wa.selectCard('VIAL') || game7wa.selectCard('PAPER') || game7wa.selectCard('CLAY') || game7wa.selectCard('STONE') || game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2'))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if ((!game7wa.hasIcon('VIAL') && !game7wa.hasIcon('PAPER') && !game7wa.hasIcon('CLAY') && !game7wa.hasIcon('STONE') && !game7wa.hasIcon('WOOD')) && (game7wa.selectCard('VIAL') || game7wa.selectCard('PAPER') || game7wa.selectCard('CLAY') || game7wa.selectCard('STONE') || game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2'))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if ((game7wa.hasIcon('VIAL') && game7wa.selectCard('VIAL')) || (game7wa.hasIcon('PAPER') && game7wa.selectCard('PAPER')) || (game7wa.hasIcon('CLAY') && game7wa.selectCard('CLAY')) || (game7wa.hasIcon('STONE') && game7wa.selectCard('STONE')) || (game7wa.hasIcon('WOOD') && (game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2')))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('POINTS3')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('CAT')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (!game7wa.hasCat()) {
                            getElementWithClass('*', 'canselect').click()
                            current = 'sevenwondersarchitects.waitingAction'
                            console.log(' >>>>>>>>> Just take random')
                        } else if (game7wa.selectCard('VIAL') || game7wa.selectCard('PAPER') || game7wa.selectCard('CLAY') || game7wa.selectCard('STONE') || game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('MATH') || game7wa.selectCard('GEAR') || game7wa.selectCard('READ')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        }
                    }
                    if (current !== 'sevenwondersarchitects.selectCard') {
                        if (!window.actions.includes('Advanced Game')) window.actions = ['Advanced Game', ...window.actions]
                        console.log(' >>>>>>>>> Advanced game')
                        return
                    }
                    // Flow in last step
                    if (game7wa.currentWonder() === 5) {
                        if ((game7wa.hasIcon('GEAR') && game7wa.hasIcon('READ') && game7wa.selectCard('MATH')) || (game7wa.hasIcon('MATH') && game7wa.hasIcon('READ') && game7wa.selectCard('GEAR')) || (game7wa.hasIcon('MATH') && game7wa.hasIcon('GEAR') && game7wa.selectCard('READ'))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('GOLD')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.hasProgressCard('IGNORE_DIFF') && (game7wa.selectCard('VIAL') || game7wa.selectCard('PAPER') || game7wa.selectCard('CLAY') || game7wa.selectCard('STONE') || game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2'))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if ((!game7wa.hasIcon('VIAL') && game7wa.selectCard('VIAL')) || (!game7wa.hasIcon('PAPER') && game7wa.selectCard('PAPER')) || (!game7wa.hasIcon('CLAY') && game7wa.selectCard('CLAY')) || (!game7wa.hasIcon('STONE') && game7wa.selectCard('STONE')) || (!game7wa.hasIcon('WOOD') && (game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2')))) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('POINTS3')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('CAT')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (!game7wa.hasCat()) {
                            getElementWithClass('*', 'canselect').click()
                            current = 'sevenwondersarchitects.waitingAction'
                            console.log(' >>>>>>>>> Just take random')
                        } else if (game7wa.selectCard('VIAL') || game7wa.selectCard('PAPER') || game7wa.selectCard('CLAY') || game7wa.selectCard('STONE') || game7wa.selectCard('WOOD') || game7wa.selectCard('WOOD2')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('MATH') || game7wa.selectCard('GEAR') || game7wa.selectCard('READ')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        }
                    }
                    // Flow additional after completed
                    if (game7wa.currentWonder() > 5) {
                        if (game7wa.selectCard('POINTS3')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (game7wa.selectCard('CAT')) {
                            current = 'sevenwondersarchitects.waitingAction'
                        } else if (!game7wa.hasCat()) {
                            getElementWithClass('*', 'canselect').click()
                            current = 'sevenwondersarchitects.waitingAction'
                            console.log(' >>>>>>>>> Just take random')
                        }
                    }
                    if (current !== 'sevenwondersarchitects.selectCard') {
                        if (!window.actions.includes('Additional after completed')) window.actions = ['Additional after completed', ...window.actions]
                        console.log(' >>>>>>>>> Additional after completed')// GOLD - REQUIRED RESOURCE - POINTS - CAT - RANDOM  - RESOURCES - GREEN
                        return
                    }
                    // Last choices
                    if (game7wa.totalAttack() - game7wa.enemyTotalAttack() >= -1 && (game7wa.selectCard('SHIELD1') || game7wa.selectCard('SHIELD2'))) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.selectCard('ATCK') || game7wa.selectCard('ATCK2')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (game7wa.selectCard('SHIELD1') || game7wa.selectCard('SHIELD2')) {
                        current = 'sevenwondersarchitects.waitingAction'
                    } else if (hasElementsWithClass('*', 'canselect')) {
                        getElementWithClass('*', 'canselect').click()
                        current = 'sevenwondersarchitects.waitingAction'
                        window.actions = ['THE HELL????', ...window.actions]
                        console.log(" >>>>>>>>> Shouldn't be here")
                    }
                    if (current !== 'sevenwondersarchitects.selectCard') {
                        window.actions = ['LAST CHOICE!?', ...window.actions]
                        console.log(' >>>>>>>>> Last choices... ATK - SHIELD ')
                    }
                }
            },
            selectResources: (n) => {
                let num = 0
                if (num !== n && hasElementsWithClassAndStyle('*', 'canselect', `background-position:${game7wa.cards.VIAL}`)) {
                    const elements = getElementsWithClassAndStyle('*', 'canselect', `background-position:${game7wa.cards.VIAL}`)
                    elements.forEach((e) => {
                        if (num !== n) {
                            e.click()
                            num++
                        }
                    })
                }
                if (num !== n && hasElementsWithClassAndStyle('*', 'canselect', `background-position:${game7wa.cards.PAPER}`)) {
                    const elements = getElementsWithClassAndStyle('*', 'canselect', `background-position:${game7wa.cards.PAPER}`)
                    elements.forEach((e) => {
                        if (num !== n) {
                            e.click()
                            num++
                        }
                    })
                }
                if (num !== n && hasElementsWithClassAndStyle('*', 'canselect', `background-position:${game7wa.cards.CLAY}`)) {
                    const elements = getElementsWithClassAndStyle('*', 'canselect', `background-position:${game7wa.cards.CLAY}`)
                    elements.forEach((e) => {
                        if (num !== n) {
                            e.click()
                            num++
                        }
                    })
                }
                if (num !== n && hasElementsWithClassAndStyle('*', 'canselect', `background-position:${game7wa.cards.STONE}`)) {
                    const elements = getElementsWithClassAndStyle('*', 'canselect', `background-position:${game7wa.cards.STONE}`)
                    elements.forEach((e) => {
                        if (num !== n) {
                            e.click()
                            num++
                        }
                    })
                }
                if (num !== n && hasElementsWithClassAndStyle('*', 'canselect', `background-position:${game7wa.cards.WOOD}`)) {
                    const elements = getElementsWithClassAndStyle('*', 'canselect', `background-position:${game7wa.cards.WOOD}`)
                    elements.forEach((e) => {
                        if (num !== n) {
                            e.click()
                            num++
                        }
                    })
                }
                if (num !== n && hasElementsWithClassAndStyle('*', 'canselect', `background-position:${game7wa.cards.WOOD2}`)) {
                    const elements = getElementsWithClassAndStyle('*', 'canselect', `background-position:${game7wa.cards.WOOD2}`)
                    elements.forEach((e) => {
                        if (num !== n) {
                            e.click()
                            num++
                        }
                    })
                }
            },
            selectSimilarGreen: (n) => {
                let selected = {}
                if (hasAtLeastNElementsWithClassAndStyle(n, '*', 'canselect', `background-position:${game7wa.cards.MATH}`)) {
                    selected = 'MATH'
                } else if (hasAtLeastNElementsWithClassAndStyle(n, '*', 'canselect', `background-position:${game7wa.cards.GEAR}`)) {
                    selected = 'GEAR'
                } else if (hasAtLeastNElementsWithClassAndStyle(n, '*', 'canselect', `background-position:${game7wa.cards.READ}`)) {
                    selected = 'READ'
                }
                if (selected !== {}) {
                    game7wa.selectCard(selected, n)
                }
            },
            selectSimilarResources: (n) => {
                let selected = {}
                if (hasAtLeastNElementsWithClassAndStyle(n, '*', 'canselect', `background-position:${game7wa.cards.VIAL}`)) {
                    selected = 'VIAL'
                } else if (hasAtLeastNElementsWithClassAndStyle(n, '*', 'canselect', `background-position:${game7wa.cards.PAPER}`)) {
                    selected = 'PAPER'
                } else if (hasAtLeastNElementsWithClassAndStyle(n, '*', 'canselect', `background-position:${game7wa.cards.CLAY}`)) {
                    selected = 'CLAY'
                } else if (hasAtLeastNElementsWithClassAndStyle(n, '*', 'canselect', `background-position:${game7wa.cards.STONE}`)) {
                    selected = 'STONE'
                } else if (hasAtLeastNElementsWithClassAndStyle(n, '*', 'canselect', `background-position:${game7wa.cards.WOOD}`)) {
                    selected = 'WOOD'
                } else if (hasAtLeastNElementsWithClassAndStyle(n, '*', 'canselect', `background-position:${game7wa.cards.WOOD2}`)) {
                    selected = 'WOOD2'
                }
                if (selected !== {}) {
                    game7wa.selectCard(selected, n)
                }
            },
            selectDifferentResources: (n) => {
                const selected = []
                while (selected.length !== n) {
                    if (!selected.includes(1) && game7wa.selectCard('VIAL')) {
                        selected.push(1)
                    } else if (!selected.includes(2) && game7wa.selectCard('PAPER')) {
                        selected.push(2)
                    } else if (!selected.includes(3) && game7wa.selectCard('CLAY')) {
                        selected.push(3)
                    } else if (!selected.includes(4) && game7wa.selectCard('STONE')) {
                        selected.push(4)
                    } else if (!selected.includes(5) && game7wa.selectCard('WOOD')) {
                        selected.push(5)
                    } else if (!selected.includes(5) && game7wa.selectCard('WOOD2')) {
                        selected.push(5)
                    }
                }
            },
            select2DifferentGreen: {
                trigger: () => {
                    states.sevenwondersarchitects.selectSimilarGreen(2)
                    if (hasSingleElementWithText('*', 'Confirm')) {
                        getElementWithText('*', 'Confirm').click()
                    }
                    current = 'sevenwondersarchitects.waitingAction'
                }
            },
            select2SimilarResources: {
                trigger: () => {
                    if (game7wa.selectCard('GOLD')) {
                        if (!game7wa.hasProgressCard('DOUBLE_GOLD')) {
                            states.sevenwondersarchitects.selectResources(1)
                        }
                    } else if (game7wa.hasProgressCard('IGNORE_DIFF')) {
                        states.sevenwondersarchitects.selectResources(2)
                    } else {
                        states.sevenwondersarchitects.selectSimilarResources(2)
                    }
                    if (hasSingleElementWithText('*', 'Confirm')) {
                        getElementWithText('*', 'Confirm').click()
                    }
                    current = 'sevenwondersarchitects.waitingAction'
                }
            },
            select2DifferentResources: {
                trigger: () => {
                    if (game7wa.selectCard('GOLD')) {
                        if (!game7wa.hasProgressCard('DOUBLE_GOLD')) {
                            states.sevenwondersarchitects.selectResources(1)
                        }
                    } else if (game7wa.hasProgressCard('IGNORE_DIFF')) {
                        states.sevenwondersarchitects.selectResources(2)
                    } else {
                        states.sevenwondersarchitects.selectDifferentResources(2)
                    }
                    if (hasSingleElementWithText('*', 'Confirm')) {
                        getElementWithText('*', 'Confirm').click()
                    }
                    current = 'sevenwondersarchitects.waitingAction'
                }
            },
            select3SimilarResources: {
                trigger: () => {
                    if (game7wa.hasProgressCard('DOUBLE_GOLD') && game7wa.selectCard('GOLD')) {
                        states.sevenwondersarchitects.selectResources(1)
                    } else if (!game7wa.hasProgressCard('DOUBLE_GOLD') && game7wa.selectCard('GOLD', 2)) {
                        states.sevenwondersarchitects.selectResources(1)
                    } else if (!game7wa.hasProgressCard('DOUBLE_GOLD') && game7wa.selectCard('GOLD')) {
                        if (game7wa.hasProgressCard('IGNORE_DIFF')) {
                            states.sevenwondersarchitects.selectResources(2)
                        } else {
                            states.sevenwondersarchitects.selectSimilarResources(2)
                        }
                    } else if (game7wa.hasProgressCard('IGNORE_DIFF')) {
                        states.sevenwondersarchitects.selectResources(3)
                    } else {
                        states.sevenwondersarchitects.selectSimilarResources(3)
                    }
                    if (hasSingleElementWithText('*', 'Confirm')) {
                        getElementWithText('*', 'Confirm').click()
                    }
                    current = 'sevenwondersarchitects.waitingAction'
                }
            },
            select3DifferentResources: {
                trigger: () => {
                    if (game7wa.hasProgressCard('DOUBLE_GOLD') && game7wa.selectCard('GOLD')) {
                        states.sevenwondersarchitects.selectResources(1)
                    } else if (!game7wa.hasProgressCard('DOUBLE_GOLD') && game7wa.selectCard('GOLD', 2)) {
                        states.sevenwondersarchitects.selectResources(1)
                    } else if (!game7wa.hasProgressCard('DOUBLE_GOLD') && game7wa.selectCard('GOLD')) {
                        if (game7wa.hasProgressCard('IGNORE_DIFF')) {
                            states.sevenwondersarchitects.selectResources(2)
                        } else {
                            states.sevenwondersarchitects.selectDifferentResources(2)
                        }
                    } else if (game7wa.hasProgressCard('IGNORE_DIFF')) {
                        states.sevenwondersarchitects.selectResources(3)
                    } else {
                        states.sevenwondersarchitects.selectDifferentResources(3)
                    }
                    if (hasSingleElementWithText('*', 'Confirm')) {
                        getElementWithText('*', 'Confirm').click()
                    }
                    current = 'sevenwondersarchitects.waitingAction'
                }
            },
            select4DifferentResources: {
                trigger: () => {
                    if (game7wa.hasProgressCard('DOUBLE_GOLD') && game7wa.selectCard('GOLD', 2)) {
                    } else if (game7wa.hasProgressCard('DOUBLE_GOLD') && game7wa.selectCard('GOLD')) {
                        if (game7wa.hasProgressCard('IGNORE_DIFF')) {
                            states.sevenwondersarchitects.selectResources(2)
                        } else {
                            states.sevenwondersarchitects.selectDifferentResources(2)
                        }
                    } else if (!game7wa.hasProgressCard('DOUBLE_GOLD') && game7wa.selectCard('GOLD', 3)) {
                        states.sevenwondersarchitects.selectResources(1)
                    } else if (!game7wa.hasProgressCard('DOUBLE_GOLD') && game7wa.selectCard('GOLD', 2)) {
                        if (game7wa.hasProgressCard('IGNORE_DIFF')) {
                            states.sevenwondersarchitects.selectResources(2)
                        } else {
                            states.sevenwondersarchitects.selectDifferentResources(2)
                        }
                    } else if (!game7wa.hasProgressCard('DOUBLE_GOLD') && game7wa.selectCard('GOLD')) {
                        if (game7wa.hasProgressCard('IGNORE_DIFF')) {
                            states.sevenwondersarchitects.selectResources(3)
                        } else {
                            states.sevenwondersarchitects.selectDifferentResources(3)
                        }
                    } else if (!game7wa.hasProgressCard('DOUBLE_GOLD')) {
                        if (game7wa.hasProgressCard('IGNORE_DIFF')) {
                            states.sevenwondersarchitects.selectResources(4)
                        } else {
                            states.sevenwondersarchitects.selectDifferentResources(4)
                        }
                    }
                    if (hasSingleElementWithText('*', 'Confirm')) {
                        getElementWithText('*', 'Confirm').click()
                    }
                    current = 'sevenwondersarchitects.waitingAction'
                }
            }
        },
        martiandice: {
            waitingAction: {
                trigger: () => {
                    if (document.title.includes('You rolled too many') && hasSingleElementWithText('*', 'end your turn')) {
                        getElementWithText('*', 'end your turn').click()
                    } else if (document.title.includes('You ') && !hasSingleElementWithText('*', 'Updating game situation...') && hasSingleElementWithId('*', 'pagemaintitletext') && getElementWithId('*', 'pagemaintitletext').innerText.trim() !== '') {
                        console.log(`MESSAGE:${getElementWithId('*', 'pagemaintitletext').innerText}`)
                        current = 'martiandice.playAction'
                    }
                }
            },
            playAction: {
                trigger: () => {
                    let elements = getElementsWithClass('*', 'play_area roll');
                    let options = {"deathray": 0, "cow": 0, "chicken": 0, "human": 0, "tank": 0}
                    elements.forEach(it => {
                        if (it.className.includes('deathray')) {
                            options["deathray"] += 1
                        } else if (it.className.includes('chicken')) {
                            options["chicken"] += 1
                        } else if (it.className.includes('cow')) {
                            options["cow"] += 1
                        } else if (it.className.includes('tank')) {
                            options["tank"] += 1
                        } else if (it.className.includes('human')) {
                            options["human"] += 1
                        }
                    })
                    window.actions = ["options: " + JSON.stringify(options)]

                    let deathRays = getElementsWithClass('*', 'die dietype_deathray roll set_aside')
                    const tanks = getElementsWithClass('*', 'die dietype_tank roll set_aside')
                    const cows = getElementsWithClass('*', 'die dietype_cow roll set_aside')
                    const chickens = getElementsWithClass('*', 'die dietype_chicken roll set_aside')
                    const humans = getElementsWithClass('*', 'die dietype_human roll set_aside')
                    const playableCows = getElementsWithClass('*', 'die dietype_cow play_area roll')
                    const playableChickens = getElementsWithClass('*', 'die dietype_chicken play_area roll')
                    const playableHumans = getElementsWithClass('*', 'die dietype_human play_area roll')
                    if (deathRays.length >= tanks.length && (cows.length + chickens.length + humans.length) >= 5 && hasSingleElementWithText('*', 'end your turn')) {
                        getElementWithText('*', 'end your turn').click()
                    } else if (hasSingleElementWithText('*', 'roll all available dice')) {
                        getElementWithText('*', 'roll all available dice').click()
                    } else if ((tanks.length > 2 || (cows.length + chickens.length + cows.length) > 2) && deathRays.length < tanks.length && hasElementsWithClass('*', 'die dietype_deathray play_area roll')) {
                        getElementWithClass('*', 'die dietype_deathray play_area roll').click()
                    } else if (playableCows.length > playableChickens.length && playableCows.length > playableHumans.length && !getElementWithClass('*', 'die dietype_cow play_area roll').className.includes('impossibleMove')) {
                        getElementWithClass('*', 'die dietype_cow play_area roll').click()
                    } else if (playableChickens.length > playableHumans.length && !getElementWithClass('*', 'die dietype_chicken play_area roll').className.includes('impossibleMove')) {
                        getElementWithClass('*', 'die dietype_chicken play_area roll').click()
                    } else if (hasElementsWithClass('*', 'die dietype_human play_area roll') && !getElementWithClass('*', 'die dietype_human play_area roll').className.includes('impossibleMove')) {
                        getElementWithClass('*', 'die dietype_human play_area roll').click()
                    } else if (hasElementsWithClass('*', 'die dietype_cow play_area roll') && !getElementWithClass('*', 'die dietype_cow play_area roll').className.includes('impossibleMove')) {
                        getElementWithClass('*', 'die dietype_cow play_area roll').click()
                    } else if (hasElementsWithClass('*', 'die dietype_chicken play_area roll') && !getElementWithClass('*', 'die dietype_chicken play_area roll').className.includes('impossibleMove')) {
                        getElementWithClass('*', 'die dietype_chicken play_area roll').click()
                    } else if (hasElementsWithClass('*', 'die dietype_deathray play_area roll') && !getElementWithClass('*', 'die dietype_deathray play_area roll').className.includes('impossibleMove')) {
                        getElementWithClass('*', 'die dietype_deathray play_area roll').click()
                    } else if (hasSingleElementWithText('*', 'end your turn')) {
                        getElementWithText('*', 'end your turn').click()
                    }
                    current = 'martiandice.waitingAction'
                }
            }
        },
        sechsnimmt: {
            waitingAction: {
                trigger: () => {
                    if (document.title.includes('You ') && !hasSingleElementWithText('*', 'Updating game situation...') && hasSingleElementWithId('*', 'pagemaintitletext') && getElementWithId('*', 'pagemaintitletext').innerText.trim() !== '') {
                        console.log(`MESSAGE:${getElementWithId('*', 'pagemaintitletext').innerText}`)
                        current = 'sechsnimmt.playAction'
                    }
                }
            },
            playAction: {
                trigger: () => {
                    if (hasElementsWithClass('*', 'takerow_btn')) {
                        getElementWithClass('*', 'takerow_btn').click()
                    } else if (hasElementsWithClass('*', 'stockitem')) {
                        const size = getElementsWithClass('*', 'stockitem').length
                        getElementsWithClass('*', 'stockitem')[Math.floor(Math.random() * size)].click()
                    }
                    current = 'sechsnimmt.waitingAction'
                }
            }
        },
        challengers: {
            waitingAction: {
                trigger: () => {
                    if ((document.title.includes('You must') || !document.title.includes(' must')) && hasSingleElementWithId('*', 'pagemaintitletext') && getElementWithId('*', 'pagemaintitletext').innerText.trim() !== 'Cards are being placed on the table' && getElementWithId('*', 'pagemaintitletext').innerText.trim() !== '') {
                        console.log(`MESSAGE:${getElementWithId('*', 'pagemaintitletext').innerText}`)
                        current = 'challengers.playAction'
                    }
                }
            },
            playAction: {
                trigger: () => {
                    if (hasSingleElementWithClass('*', 'fa fa-close')) {
                        getElementWithClass('*', 'fa fa-close').click()
                        current = 'started'
                    } else if (hasSingleElementWithText('*', 'Begin Deck Phase')) {
                        getElementWithText('*', 'Begin Deck Phase').click()
                    } else if (hasSingleElementWithText('*', 'Reveal Top Card')) {
                        getElementWithText('*', 'Reveal Top Card').click()
                    } else if (hasSingleElementWithText('*', 'Continue')) {
                        getElementWithText('*', 'Continue').click()
                    } else if (hasSingleElementWithText('*', 'Confirm')) {
                        getElementWithText('*', 'Confirm').click()
                    }
                    current = 'challengers.waitingAction'
                }
            }
        },
        cantstop: {
            waitingAction: {
                trigger: () => {
                    window.refreshCount = 50
                    if (document.title.includes('You must') && hasSingleElementWithId('*', 'pagemaintitletext') && getElementWithId('*', 'pagemaintitletext').innerText.trim() !== '') {
                        console.log(`MESSAGE:${getElementWithId('*', 'pagemaintitletext').innerText}`)
                        current = 'cantstop.playAction'
                    } else if (document.title.includes(' must')) {
                        if (hasSingleElementWithId('*', 'pagemaintitletext')) {
                            cantstop.temp_progress = {...cantstop.progress}
                            cantstop.selected = []
                            cantstop.retry = 0
                            cantstop.total_progress = 0
                        }
                    }
                }
            },
            playAction: {
                trigger: () => {
                    if (cantstop.selected.length === 0) {
                        window.actions = []
                    }

                    const min_expected_progress = 1.2 * cantstop.selected.map(n => cantstop.max[n] / 13).reduce((sum, a) => sum + a, 0) / cantstop.selected.length
                    const canFinishColumn = cantstop.selected
                        .map(n => (cantstop.temp_progress[n] + 1) >= cantstop.max[n]).reduce((res, a) => a ? a : res, false);
                    let hope = false
                    if (canFinishColumn && cantstop.total_progress > min_expected_progress && cantstop.total_progress < min_expected_progress + 0.3)
                        hope = true
                    if (cantstop.finishedColumn() && hasElementWithText('*', 'Stop')) {
                        getElementWithText('*', 'Stop').click()
                        cantstop.progress = {...cantstop.temp_progress}
                    } else if (hasElementWithText('*', 'Progresses on')) {
                        const bestString = cantstop.bestElementString()
                        getElementWithText('*', bestString).click()
                        cantstop.addProgress(bestString);
                        cantstop.retry = cantstop.retry + 1
                        cantstop.retryLimit = cantstop.selected.map(n => cantstop.max[n]).reduce((min, a) => (a < min) ? a : min, 13)
                        if (cantstop.total_progress < min_expected_progress) cantstop.retryLimit = cantstop.retry + 1
                        else cantstop.retryLimit = 1
                        window.actions = [" Progress: " + (hope ? "(hope) " : "") + Math.round(cantstop.total_progress * 100) + "% / " + Math.round(min_expected_progress * 100) + "%", ...window.actions]
                    } else if (hope || cantstop.selected.length < 3 || (cantstop.retry > 0 && cantstop.retry < cantstop.retryLimit && hasElementWithText('*', 'Continue'))) {
                        getElementWithText('*', 'Continue').click()
                    } else if (hasElementWithText('*', 'Stop')) {
                        getElementWithText('*', 'Stop').click()
                        cantstop.progress = {...cantstop.temp_progress}
                    }
                    current = 'cantstop.waitingAction'
                }
            }
        },
        marrakech: {
            waitingAction: {
                trigger: () => {
                    if ((document.title.includes('You may') || !document.title.includes(' may')) && hasSingleElementWithId('*', 'pagemaintitletext') && getElementWithId('*', 'pagemaintitletext').innerText.trim() !== 'Cards are being placed on the table' && getElementWithId('*', 'pagemaintitletext').innerText.trim() !== '') {
                        console.log(`MESSAGE:${getElementWithId('*', 'pagemaintitletext').innerText}`)
                        current = 'marrakech.playAction'
                    }
                }
            },
            playAction: {
                trigger: () => {
                    if (hasElementWithText('*', 'Roll die')) {
                        getElementWithText('*', 'Roll die').click()
                    } else if (hasElementWithText('*', 'Validate')) {
                        getElementWithText('*', 'Validate').click()
                    }
                    current = 'marrakech.waitingAction'
                }
            }
        },
        barenpark: {
            waitingAction: {
                trigger: () => {
                    if ((document.title.includes('You may') || !document.title.includes(' may')) && hasSingleElementWithId('*', 'pagemaintitletext') && getElementWithId('*', 'pagemaintitletext').innerText.trim() !== 'Cards are being placed on the table' && getElementWithId('*', 'pagemaintitletext').innerText.trim() !== '') {
                        console.log(`MESSAGE:${getElementWithId('*', 'pagemaintitletext').innerText}`)
                        current = 'barenpark.playAction'
                    }
                }
            },
            playAction: {
                trigger: () => {
                    if (hasElementWithText('*', 'Prepare next turn')) {
                        getElementWithText('*', 'Prepare next turn').click()
                    } else if (hasElementWithText('*', 'Confirm')) {
                        getElementWithText('*', 'Confirm').click()
                    }
                    current = 'barenpark.waitingAction'
                }
            }
        },
        festival: {
            waitingAction: {
                trigger: () => {
                    if (document.title.includes('You must') && hasSingleElementWithId('*', 'pagemaintitletext') && getElementWithId('*', 'pagemaintitletext').innerText.trim() !== '') {
                        console.log(`MESSAGE:${getElementWithId('*', 'pagemaintitletext').innerText}`)
                        current = 'festival.playAction'
                    }
                }
            },
            playAction: {
                trigger: () => {
                    if (hasElementWithText('*', 'Confirm')) {
                        getElementWithText('*', 'Confirm').click()
                    }
                    current = 'festival.waitingAction'
                }
            }
        }
    }

    let count = 0
    setInterval(() => {
        if (count++ < window.refreshCount) {
            return
        }
        count = 0
        try {
            if (!current.includes("waiting")) {
                console.log(`Executing status: ${current}`)
            }
            let originalState = current
            const mainPart = eval(states[current.substring(current.lastIndexOf('.') + 1)])
            if (mainPart) mainPart.trigger()

            if (originalState === current) {
                eval(`states['${current.replace('.', "']['")}']`).trigger()
            }
        } catch (e) {
            // console.log(`Error in state ${current}: ${e}`)
            current = 'started'
        }
    }, window.refreshTime)
}())
